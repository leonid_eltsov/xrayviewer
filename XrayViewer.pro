#-------------------------------------------------
#
# Project created by QtCreator 2019-06-28T09:05:49
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = XrayViewer
TEMPLATE = app


SOURCES += main.cpp\
        xrayviewer.cpp \
    brightcontrastdialog.cpp \
    imageprocessor.cpp

HEADERS  += xrayviewer.h \
    brightcontrastdialog.h \
    imageprocessor.h

FORMS    += xrayviewer.ui \
    brightcontrastdialog.ui
