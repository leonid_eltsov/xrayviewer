/********************************************************************************
** Form generated from reading UI file 'brightcontrastdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_BRIGHTCONTRASTDIALOG_H
#define UI_BRIGHTCONTRASTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSlider>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_BrightContrastDialog
{
public:
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QSlider *BrightnessSlider;
    QSlider *ContrastSlider;
    QLabel *label;
    QLabel *label_2;

    void setupUi(QDialog *BrightContrastDialog)
    {
        if (BrightContrastDialog->objectName().isEmpty())
            BrightContrastDialog->setObjectName(QStringLiteral("BrightContrastDialog"));
        BrightContrastDialog->resize(359, 109);
        gridLayoutWidget = new QWidget(BrightContrastDialog);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 10, 341, 91));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        BrightnessSlider = new QSlider(gridLayoutWidget);
        BrightnessSlider->setObjectName(QStringLiteral("BrightnessSlider"));
        BrightnessSlider->setMinimum(0);
        BrightnessSlider->setMaximum(100);
        BrightnessSlider->setSingleStep(1);
        BrightnessSlider->setValue(50);
        BrightnessSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(BrightnessSlider, 0, 1, 1, 1);

        ContrastSlider = new QSlider(gridLayoutWidget);
        ContrastSlider->setObjectName(QStringLiteral("ContrastSlider"));
        ContrastSlider->setMinimum(0);
        ContrastSlider->setMaximum(100);
        ContrastSlider->setSingleStep(1);
        ContrastSlider->setPageStep(10);
        ContrastSlider->setValue(50);
        ContrastSlider->setOrientation(Qt::Horizontal);

        gridLayout->addWidget(ContrastSlider, 1, 1, 1, 1);

        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        label_2 = new QLabel(gridLayoutWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        gridLayout->addWidget(label_2, 1, 0, 1, 1);


        retranslateUi(BrightContrastDialog);

        QMetaObject::connectSlotsByName(BrightContrastDialog);
    } // setupUi

    void retranslateUi(QDialog *BrightContrastDialog)
    {
        BrightContrastDialog->setWindowTitle(QApplication::translate("BrightContrastDialog", "Brightness and Contrast", 0));
        label->setText(QApplication::translate("BrightContrastDialog", "Brightness:", 0));
        label_2->setText(QApplication::translate("BrightContrastDialog", "Contrast:", 0));
    } // retranslateUi

};

namespace Ui {
    class BrightContrastDialog: public Ui_BrightContrastDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BRIGHTCONTRASTDIALOG_H
