#include "xrayviewer.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    XrayViewer w;
    w.show();

    return a.exec();
}
