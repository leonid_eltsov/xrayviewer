#ifndef IMAGEPROCESSOR_H
#define IMAGEPROCESSOR_H

#include <QObject>
#include <QImage>


class ImageProcessor:public QObject
{
    Q_OBJECT

     // Слишком много копирования. QSharedDataPointer/QSharedPointer? + img в heap.

    QImage img;
    float alpha, beta;

public:
    ImageProcessor(QImage, float, float);

signals:
void Processed(QImage);
void Finished (void);

public slots:
void LinearTransform(void);
};

#endif // IMAGEPROCESSOR_H
