/********************************************************************************
** Form generated from reading UI file 'xrayviewer.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_XRAYVIEWER_H
#define UI_XRAYVIEWER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_XrayViewer
{
public:
    QAction *actionOpen;
    QAction *actionBrightness_contrast;
    QAction *actionQuit;
    QAction *actionAbout;
    QWidget *centralWidget;
    QLabel *ImageLabel;
    QMenuBar *menuBar;
    QMenu *menuOpen_file;
    QMenu *menuBrightness_contrast;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *XrayViewer)
    {
        if (XrayViewer->objectName().isEmpty())
            XrayViewer->setObjectName(QStringLiteral("XrayViewer"));
        XrayViewer->resize(1024, 560);
        XrayViewer->setMouseTracking(true);
        actionOpen = new QAction(XrayViewer);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionBrightness_contrast = new QAction(XrayViewer);
        actionBrightness_contrast->setObjectName(QStringLiteral("actionBrightness_contrast"));
        actionQuit = new QAction(XrayViewer);
        actionQuit->setObjectName(QStringLiteral("actionQuit"));
        actionAbout = new QAction(XrayViewer);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        centralWidget = new QWidget(XrayViewer);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setMouseTracking(true);
        ImageLabel = new QLabel(centralWidget);
        ImageLabel->setObjectName(QStringLiteral("ImageLabel"));
        ImageLabel->setGeometry(QRect(0, 0, 1024, 512));
        ImageLabel->setMouseTracking(true);
        ImageLabel->setAlignment(Qt::AlignCenter);
        XrayViewer->setCentralWidget(centralWidget);
        ImageLabel->raise();
        menuBar = new QMenuBar(XrayViewer);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1024, 21));
        menuBar->setMouseTracking(false);
        menuOpen_file = new QMenu(menuBar);
        menuOpen_file->setObjectName(QStringLiteral("menuOpen_file"));
        menuOpen_file->setMouseTracking(true);
        menuBrightness_contrast = new QMenu(menuBar);
        menuBrightness_contrast->setObjectName(QStringLiteral("menuBrightness_contrast"));
        XrayViewer->setMenuBar(menuBar);
        statusBar = new QStatusBar(XrayViewer);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        XrayViewer->setStatusBar(statusBar);

        menuBar->addAction(menuOpen_file->menuAction());
        menuBar->addAction(menuBrightness_contrast->menuAction());
        menuOpen_file->addAction(actionOpen);
        menuOpen_file->addAction(actionBrightness_contrast);
        menuOpen_file->addAction(actionQuit);
        menuBrightness_contrast->addAction(actionAbout);

        retranslateUi(XrayViewer);

        QMetaObject::connectSlotsByName(XrayViewer);
    } // setupUi

    void retranslateUi(QMainWindow *XrayViewer)
    {
        XrayViewer->setWindowTitle(QApplication::translate("XrayViewer", "XrayViewer", 0));
        actionOpen->setText(QApplication::translate("XrayViewer", "Open\342\200\246", 0));
        actionBrightness_contrast->setText(QApplication::translate("XrayViewer", "Brightness/contrast\342\200\246", 0));
        actionQuit->setText(QApplication::translate("XrayViewer", "Quit", 0));
        actionAbout->setText(QApplication::translate("XrayViewer", "About", 0));
        ImageLabel->setText(QApplication::translate("XrayViewer", "NoImage", 0));
        menuOpen_file->setTitle(QApplication::translate("XrayViewer", "File", 0));
        menuBrightness_contrast->setTitle(QApplication::translate("XrayViewer", "Help", 0));
    } // retranslateUi

};

namespace Ui {
    class XrayViewer: public Ui_XrayViewer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_XRAYVIEWER_H
