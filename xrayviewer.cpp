#include "xrayviewer.h"
#include "ui_xrayviewer.h"
#include "imageprocessor.h"

#include <QDebug>
#include <QString>
#include <QFile>
#include <QFileDialog>
#include <Qthread>

/*
#include <fstream>
#include <iterator>
#include <vector>
*/

XrayViewer::XrayViewer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::XrayViewer)
{
    XrayImage = new QImage(img_width, img_height, QImage::Format_Grayscale8);
    ProcessedImage = new QImage(img_width, img_height, QImage::Format_Grayscale8);;

    ui->setupUi(this);
    ui->actionBrightness_contrast->setEnabled(false);
}

XrayViewer::~XrayViewer()
{
    delete ui;
}


void XrayViewer::mouseMoveEvent(QMouseEvent *event)
{
    int x = event->pos().x();
    int y = event->pos().y()- menuBar()->geometry().height();

    if ( XrayImage->valid(x,y) )
    {
        int g = qGray( XrayImage->pixel(x,y) );
        ui->statusBar->showMessage( "x: " + QString::number(x) + "    y: " + QString::number(y) + "    g: " + QString::number(g) );
    }
}


QByteArray XrayViewer::Normalize (const QVector<quint32> &Vec32)
{
    QByteArray Normalized(img_size, 0);

    if(Vec32.size() != img_size) return Normalized;

    quint32 max = *std::max_element(Vec32.begin(), Vec32.end());

    for(int i = 0; i < img_size; ++i)
        Normalized[i] = round ( ( (Vec32[i])*255 )/(float)max );

    return Normalized;
}

void XrayViewer::SetImage (const QByteArray &arr)
{
    if (arr.size() != img_size) return;

    int k = 0;
    for(int i = 0; i < img_width; ++i)
        for(int j = 0; j < img_height; ++j)
        {
            int g = arr[k++];
            XrayImage->setPixel(i,j, qRgb(g,g,g));
        }

    ui->ImageLabel->setPixmap(QPixmap::fromImage(*XrayImage));
}

void XrayViewer::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Open Image", "/home/data", "Image Files (*.raw)");

    /*
    std::ifstream input( fileName.toStdString(), std::ios::binary );
    std::vector<unsigned char> buffer(std::istreambuf_iterator<char>(input), {});
    std::vector<uint32_t> Vec32(img_size*4);
    std::copy(buffer.begin(), buffer.end(), reinterpret_cast<char*>(Vec32.data()));
    */

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))    return;
    if (file.size() != img_size*4)          return;

    QByteArray Raw = file.readAll();
    QVector<quint32> Vec32(img_size);

    int j = 0;
    for(int i = 0; i < Raw.size(); i+=4)
    {
        quint32 tmp32 = 0;

        tmp32  = Raw[i] & 0xFF;
        tmp32 |= (Raw[i+1] << 8) & 0xFFFF;
        tmp32 |= (Raw[i+2] << 16) & 0xFFFFFF;
        tmp32 |= (Raw[i+3] << 24) & 0xFFFFFFFF;

        Vec32[j++] = tmp32;
    }

    SetImage( Normalize(Vec32) );

    ui->actionBrightness_contrast->setEnabled(true);
}

void XrayViewer::ChangeBrightnessContrast(int brightness, int contrast)
{
    float alpha = brightness/50.0;
    float beta = (contrast-50)*255.0/50.0;

    QThread *thread= new QThread;
    ImageProcessor *ImgProc = new ImageProcessor(*XrayImage, alpha, beta);

    ImgProc->moveToThread(thread);

    connect(thread, SIGNAL(started()), ImgProc, SLOT(LinearTransform()));
    connect(ImgProc, SIGNAL(Processed(QImage)), this, SLOT(UpdateImage(QImage)));

    connect(ImgProc, SIGNAL(Finished()), ImgProc, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
    connect(ImgProc, SIGNAL(Finished()), thread, SLOT(quit()));

    thread->start();
}


void XrayViewer::UpdateImage (QImage img)
{
    ui->ImageLabel->setPixmap(QPixmap::fromImage(img));
}

void XrayViewer::ShowOriginalImage (void)
{
    ui->ImageLabel->setPixmap(QPixmap::fromImage(*XrayImage));
}

void XrayViewer::on_actionBrightness_contrast_triggered()
{
    BrightContrDialog = new BrightContrastDialog(this);

    BrightContrDialog->setAttribute(Qt::WA_DeleteOnClose);

    connect(BrightContrDialog, SIGNAL (BrightnessContrastChanged(int, int)), this, SLOT (ChangeBrightnessContrast(int, int)) );
    connect(BrightContrDialog, SIGNAL (destroyed()), this, SLOT (ShowOriginalImage()));

    BrightContrDialog->exec();
}

void XrayViewer::on_actionQuit_triggered()
{
    QApplication::quit();
}
