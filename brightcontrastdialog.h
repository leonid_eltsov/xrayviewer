#ifndef BRIGHTCONTRASTDIALOG_H
#define BRIGHTCONTRASTDIALOG_H

#include <QDialog>
#include <QDebug>

namespace Ui {
class BrightContrastDialog;
}

class BrightContrastDialog : public QDialog
{
    Q_OBJECT

public:
    explicit BrightContrastDialog(QWidget *parent = 0);
    ~BrightContrastDialog();

private:
    Ui::BrightContrastDialog *ui;

private slots:
 void ChangeBrightness(int brightness);
 void ChangeContrast(int contrast);

signals:
 void BrightnessContrastChanged(int brightness, int contrast);


};

#endif // BRIGHTCONTRASTDIALOG_H
