#include "imageprocessor.h"
#include <QDebug>



ImageProcessor::ImageProcessor(QImage i, float a, float b):img(i), alpha(a), beta(b)
{

}

void ImageProcessor::LinearTransform(void)
{
    for(int i = 0; i < img.width(); ++i)
        for(int j = 0; j < img.height(); ++j)
        {
            int px = round(qGray(img.pixel(i,j))*alpha + beta);

            if(px > 255) px = 255;
            if(px < 0) px = 0;

            img.setPixel(i,j, qRgb(px,px,px));
        }

    emit Processed(img);
    emit Finished();
}
