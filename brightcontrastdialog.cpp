#include "brightcontrastdialog.h"
#include "ui_brightcontrastdialog.h"

BrightContrastDialog::BrightContrastDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BrightContrastDialog)
{
    ui->setupUi(this);

    connect(ui->BrightnessSlider, SIGNAL (valueChanged(int)), this, SLOT (ChangeBrightness(int)) );
    connect(ui->ContrastSlider, SIGNAL (valueChanged(int)), this, SLOT (ChangeContrast(int)) );
}


void BrightContrastDialog::ChangeBrightness(int brightness)
{
   emit BrightnessContrastChanged(brightness, ui->ContrastSlider->value());
}
void BrightContrastDialog::ChangeContrast(int contrast)
{
   emit BrightnessContrastChanged(ui->BrightnessSlider->value(), contrast);
}

BrightContrastDialog::~BrightContrastDialog()
{
    delete ui;
}
