#ifndef XRAYVIEWER_H
#define XRAYVIEWER_H

#include <QMainWindow>
#include <QMouseEvent>
#include <QVector>
#include <QByteArray>
#include "brightcontrastdialog.h"

namespace Ui {
class XrayViewer;
}

class XrayViewer : public QMainWindow
{
    Q_OBJECT

public:
    explicit XrayViewer(QWidget *parent = 0);
    ~XrayViewer();

private:
    Ui::XrayViewer *ui;

    QImage *XrayImage;
    QImage *ProcessedImage;

    BrightContrastDialog *BrightContrDialog;

    const int img_height = 512;
    const int img_width = 1024;
    const int img_size = img_height*img_width;

    QByteArray Normalize (const QVector<quint32> &raw);
    void SetImage (const QByteArray &arr);

protected:
    void mouseMoveEvent(QMouseEvent *event) override;
private slots:
    void on_actionOpen_triggered();
    void on_actionBrightness_contrast_triggered();
    void on_actionQuit_triggered();

    void ChangeBrightnessContrast(int brightness, int contrast);
    void ShowOriginalImage(void);

    void UpdateImage (QImage img);

};

#endif // XRAYVIEWER_H
